#!/usr/bin/env python3
import pygame
from pyo import Server, Mixer, Sine, pa_count_devices
import os
os.environ["SDL_AUDIODRIVER"] = "dummy"

notes = {
    pygame.K_z: 0,
    pygame.K_s: 1,
    pygame.K_x: 2,
    pygame.K_d: 3,
    pygame.K_c: 4,
    pygame.K_v: 5,
    pygame.K_g: 6,
    pygame.K_b: 7,
    pygame.K_h: 8,
    pygame.K_n: 9,
    pygame.K_j: 10,
    pygame.K_m: 11,
    pygame.K_COMMA: 12,
    pygame.K_l: 13,
    pygame.K_PERIOD: 14,
    pygame.K_SEMICOLON: 15,
    pygame.K_SLASH: 16,
}


def get_server():
  devices = pa_count_devices()
  for i in range(devices):
    s = Server()
    s.setOutputDevice(i)
    try:
      s.boot().start()
      print(s)
      print("got a server from output %d" % i)
      if s.getNchnls() > 0:
        return s
    except:
      pass

def main():
    s = get_server()
    voices = []
    VOICE_TOTAL = 12
    mixer = Mixer()
    for i in range(VOICE_TOTAL):
        gen = Sine().stop()
        voices.append(gen)
        mixer.addInput(i, gen)
        mixer.setAmp(i, 0, 0.5)
        mixer.setAmp(i, 1, 0.5)
    mixer.out()
    pygame.init()
    pygame.display.set_mode((500, 500))
    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT:
                return
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_ESCAPE]:
            return
        chord = []
        for note in (notes[key] for key in notes if pressed[key]):
            chord.append(note)
        #    print(chord)
        voice = 0
        for i in chord:
            v = voices[voice]
# fix intonation, or mess it up more, or make it "changeable" by other key presses!
            v.setFreq(440 + (440 / 12) * (i + 1))
            v.play()
            voice += 1
        while voice < VOICE_TOTAL:
            v = voices[voice]
            v.stop()
            voice += 1
        pygame.display.update()


if __name__ == "__main__":
    main()
